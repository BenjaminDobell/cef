diff --git base/base_paths_posix.cc base/base_paths_posix.cc
index 048434f..6b4cc0d 100644
--- base/base_paths_posix.cc
+++ base/base_paths_posix.cc
@@ -27,8 +27,30 @@
 #include <stdlib.h>
 #endif
 
+#include <dlfcn.h>
+
 namespace base {
 
+  static const char * s_path = 0;
+
+  __attribute__((constructor))
+  void InitSharedLibPath()
+  {
+    Dl_info dl_info;
+    if (dladdr((void*)InitSharedLibPath, &dl_info)) {
+      std::string path = FilePath(dl_info.dli_fname).DirName().value();
+      s_path = new char[path.length()+1];
+      memcpy((void*)s_path, path.c_str(), path.length()+1);
+    }
+  }
+
+  __attribute__((destructor))
+  void DeinitSharedLibPath()
+  {
+    delete[] s_path;
+    s_path = 0;
+  }
+
 bool PathProviderPosix(int key, FilePath* result) {
   FilePath path;
   switch (key) {
@@ -109,6 +131,12 @@ bool PathProviderPosix(int key, FilePath* result) {
       *result = cache_dir;
       return true;
     }
+    case base::DIR_SHARED_LIB:
+      if (s_path) {
+        *result = FilePath(s_path);
+        return true;
+      }
+      return false;
   }
   return false;
 }
diff --git base/base_paths_posix.h base/base_paths_posix.h
index ef002ae..87e151b 100644
--- base/base_paths_posix.h
+++ base/base_paths_posix.h
@@ -19,6 +19,10 @@ enum {
                 // browser cache can be a subdirectory.
                 // This is $XDG_CACHE_HOME on Linux and
                 // ~/Library/Caches on Mac.
+
+  DIR_SHARED_LIB, // If compiled as a shared library, the location of
+                  // the file.
+
   PATH_POSIX_END
 };
 
diff --git base/i18n/icu_util.cc base/i18n/icu_util.cc
index 8bbbc04..345fc94 100644
--- base/i18n/icu_util.cc
+++ base/i18n/icu_util.cc
@@ -11,6 +11,7 @@
 #include <string>
 
 #include "base/files/file_path.h"
+#include "base/files/file_util.h"
 #include "base/files/memory_mapped_file.h"
 #include "base/logging.h"
 #include "base/path_service.h"
@@ -151,6 +152,14 @@ bool InitializeICU() {
       return false;
     }
 #endif  // OS check
+
+#if defined(OS_POSIX)
+    if (!base::PathExists(data_path)) {
+      PathService::Get(base::DIR_SHARED_LIB, &data_path);
+      data_path = data_path.AppendASCII(kIcuDataFileName);
+    }
+#endif
+
     if (!mapped_file.Initialize(data_path)) {
       LOG(ERROR) << "Couldn't mmap " << data_path.AsUTF8Unsafe();
       return false;
diff --git gin/v8_initializer.cc gin/v8_initializer.cc
index 67b5f88..055d862 100644
--- gin/v8_initializer.cc
+++ gin/v8_initializer.cc
@@ -33,7 +33,7 @@ const int kV8SnapshotBasePathKey =
 #if defined(OS_ANDROID)
     base::DIR_ANDROID_APP_DATA;
 #elif defined(OS_POSIX)
-    base::DIR_EXE;
+    base::DIR_SHARED_LIB;
 #elif defined(OS_WIN)
     base::DIR_MODULE;
 #endif  // OS_ANDROID
diff --git media/base/media_posix.cc media/base/media_posix.cc
index f8f0c99..522c9b4 100644
--- media/base/media_posix.cc
+++ media/base/media_posix.cc
@@ -8,6 +8,7 @@
 
 #include "base/files/file_path.h"
 #include "base/logging.h"
+#include "base/path_service.h"
 #include "base/strings/stringize_macros.h"
 #include "media/ffmpeg/ffmpeg_common.h"
 #include "third_party/ffmpeg/ffmpeg_stubs.h"
@@ -51,6 +52,11 @@ bool InitializeMediaLibraryInternal(const base::FilePath& module_dir) {
   DCHECK_EQ(kNumStubModules, 1);
   paths[kModuleFfmpegsumo].push_back(module_dir.Append(kSumoLib).value());
 
+  base::FilePath lib_dir;
+  if (PathService::Get(base::DIR_SHARED_LIB, &lib_dir)) {
+    paths[kModuleFfmpegsumo].push_back(lib_dir.Append(kSumoLib).value());
+  }
+
   // If that fails, see if any system libraries are available.
   paths[kModuleFfmpegsumo].push_back(module_dir.Append(
       FILE_PATH_LITERAL(DSO_NAME("avutil", AVUTIL_VERSION))).value());
diff --git sandbox/linux/suid/client/setuid_sandbox_host.cc sandbox/linux/suid/client/setuid_sandbox_host.cc
index 71171eb..37fba40 100644
--- sandbox/linux/suid/client/setuid_sandbox_host.cc
+++ sandbox/linux/suid/client/setuid_sandbox_host.cc
@@ -128,6 +128,13 @@ base::FilePath SetuidSandboxHost::GetSandboxBinaryPath() {
       sandbox_binary = sandbox_candidate;
   }
 
+  base::FilePath lib_dir;
+  if (sandbox_binary.empty() && PathService::Get(base::DIR_SHARED_LIB, &lib_dir)) {
+    base::FilePath sandbox_candidate = lib_dir.AppendASCII("chrome-sandbox");
+    if (base::PathExists(sandbox_candidate))
+      sandbox_binary = sandbox_candidate;
+  }
+
   // In user-managed builds, including development builds, an environment
   // variable is required to enable the sandbox. See
   // http://code.google.com/p/chromium/wiki/LinuxSUIDSandboxDevelopment
diff --git chrome/common/chrome_paths.cc chrome/common/chrome_paths.cc
index b5fd5b5..67fd59a 100644
--- chrome/common/chrome_paths.cc
+++ chrome/common/chrome_paths.cc
@@ -95,7 +95,7 @@ bool GetInternalPluginsDirectory(base::FilePath* result) {
 #endif
 
   // The rest of the world expects plugins in the module directory.
-  return PathService::Get(base::DIR_MODULE, result);
+  return PathService::Get(base::DIR_SHARED_LIB, result);
 }
 
 #if defined(OS_WIN)
